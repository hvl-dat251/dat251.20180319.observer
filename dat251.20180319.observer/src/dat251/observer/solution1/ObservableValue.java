package dat251.observer.solution1;

import java.util.Observable;

public class ObservableValue extends Observable {

   private int n;

   public void setValue(int n) {
      this.n = n;
      setChanged();
      notifyObservers();
   }

   public int getValue() {
      return n;
   }
}

