package dat251.observer.solution3;
/*
 * Banksjef.java
 *
 * Created on 10. september 2002, 14:22
 */


/**
 *
 * @author  anderl
 * @version 
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class Banksjef extends JFrame {

    /** Creates new Banksjef */
    public Banksjef() {
        super("Banksjefens tall");
        
        Konto konto1 = new Konto("Konto 1", 1000.00);
        JPanel k1 = lagKontoPanel(konto1);
        
        Konto konto2 = new Konto("Konto 2", 7000.00);
        JPanel k2 = lagKontoPanel(konto2); 
        
        getContentPane().setLayout(new GridLayout(2,1));
        getContentPane().add(k1);
        getContentPane().add(k2);
        
        setSize(400,500);
    }
    
    private JPanel lagKontoPanel(Konto k) {
       JPanel kontoPanel = new JPanel();
       
       kontoPanel.setBorder(new TitledBorder(k.getKontoNavn()));
       
       AbstractKontoUtsnitt kontUtsnitt1 = new KontoUtsnitt1(k);
       AbstractKontoUtsnitt kontUtsnitt2 = new KontoUtsnitt2(k);
       
       KontoKontroller kontKontr = new KontoKontroller(k);
       
       kontoPanel.add(kontKontr);
       kontoPanel.add(kontUtsnitt1);
       kontoPanel.add(kontUtsnitt2);
       
       return kontoPanel; 
    }
    
    public static void main (String args[]) {
        Banksjef bank = new Banksjef();
        bank.setDefaultCloseOperation(EXIT_ON_CLOSE);
        bank.setVisible(true);
    }

}
