package dat251.observer.solution3;
/*
 * AbstractKontoUtsnitt.java
 *
 * Created on 10. september 2002, 13:03
 */


/**
 *
 * @author  anderl
 * @version 
 */
import java.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

public abstract class AbstractKontoUtsnitt extends JPanel
   implements Observer {

    private Konto konto;
    
    /** Creates new AbstractKontoUtsnitt */
    public AbstractKontoUtsnitt(Konto k) 
            throws NullPointerException {
        if (k == null)
               throw new NullPointerException();
        
        //sett konto refereanse
        konto = k;
        
        // registrer som en Observer
        konto.addObserver(this);
        
        //visningsegenskaper
        setBackground(Color.white);
        setBorder(new MatteBorder(1,1,1,1,Color.black));
    }
    
    public Konto getKonto() {
        return konto;
    }

    // mottar melding om oppdateringer Konto (som er Observable)
    public void update(Observable obs, Object obj) { 
        updateDisplay();
    }
    
    protected abstract void updateDisplay();
    
}
