package dat251.observer.solution3;
/*
 * KontoUtsnitt2.java
 *
 * Created on 10. september 2002, 13:51
 */


/**
 *
 * @author  anderl
 * @version 
 */
import java.awt.*; //Font
import java.util.*;
import java.text.DecimalFormat;
import javax.swing.*;

public class KontoUtsnitt2 extends AbstractKontoUtsnitt {

    private JTextArea saldoTekstOmråde = new JTextArea(3,25);
    private DecimalFormat toSiffer = new DecimalFormat("0.00");
    
    /** Creates new KontoUtsnitt1 */
    public KontoUtsnitt2(Konto konto) {
        super(konto);
        // ikke oppdatering av tekstfelt
        saldoTekstOmråde.setEditable(false);
        
        //legg ut komponenten
        add(saldoTekstOmråde);
        updateDisplay();
    }
    
    public void updateDisplay() {
        // hent saldo med metoden getSaldo som
        //arves fra AbstractKontoUtsnitt
        saldoTekstOmråde.setFont(
             new Font("Monospaced", Font.BOLD, 20));
        saldoTekstOmråde.setText("View 2:\n" +
                                 "Saldoen på " +
                                 getKonto().getKontoNavn() +
                                 " er på:\n" +
             toSiffer.format(getKonto().getSaldo()));
    }

}
