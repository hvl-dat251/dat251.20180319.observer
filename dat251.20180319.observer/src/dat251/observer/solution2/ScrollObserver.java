package dat251.observer.solution2;

import java.awt.*;
import java.util.Observer;
import java.util.Observable;

@SuppressWarnings("serial")
public class ScrollObserver extends Frame implements Observer {

	private ObservableValue ov = null;

	private Scrollbar sb = null;

	public ScrollObserver(ObservableValue ov) {
		super("Scroll Observer Tool");
		this.ov = ov;

		setLayout(new GridLayout(0, 1));
		sb = new Scrollbar(Scrollbar.HORIZONTAL,
				ov.getValue(), 10,
				ov.getLowerBound(),
				ov.getHigherBound());
		add(sb);
		pack();
		setVisible(true);
	}

	@SuppressWarnings("deprecation")
	public boolean handleEvent(Event evt) {

		switch (evt.id) {
		case Event.SCROLL_LINE_UP:
		case Event.SCROLL_LINE_DOWN:
		case Event.SCROLL_PAGE_UP:
		case Event.SCROLL_PAGE_DOWN:
		case Event.SCROLL_ABSOLUTE:
			ov.setValue(sb.getValue());
			return true;
		case Event.WINDOW_DESTROY:
			ov.deleteObserver(this);
			dispose();	   
			return true;
		}

		return super.handleEvent(evt);
	}

	public void update(Observable obs, Object obj) {
		if (obs == ov) {
			sb.setValue(ov.getValue());
		}
	}
}
