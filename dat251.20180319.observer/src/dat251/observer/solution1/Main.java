package dat251.observer.solution1;

public class Main {

	public static void main(String[] args) {
		
		ObservableValue ov = new ObservableValue();
		new TextObserver(ov);

		for (int i = 0; i < 10; i++) {
			ov.setValue(i);
		}
	}
}
