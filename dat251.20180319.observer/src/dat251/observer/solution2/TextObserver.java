package dat251.observer.solution2;

import java.awt.Event;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.TextField;
import java.util.Observable;
import java.util.Observer;

@SuppressWarnings("serial")
public class TextObserver extends Frame implements Observer {

	private ObservableValue ov = null;

	private TextField tf = null;
	private Label l = null;

	private int nLow = 0;
	private int nHigh = 0;

	public TextObserver(ObservableValue ov) {
		super("Text Observer Tool");
		this.ov = ov;

		setLayout(new GridLayout(0, 1));
		nLow = ov.getLowerBound();
		nHigh = ov.getHigherBound();
		tf = new TextField(String.valueOf(ov.getValue()));
		add(tf);
		l = new Label();
		add(l);
		pack();
		setVisible(true);
	}

	public boolean action(Event evt, Object obj) {

		if (evt.target == tf) {
			int n = 0;
			boolean boolValid = false;

			try {
				n = Integer.parseInt(tf.getText());
				boolValid = true;

			} catch (NumberFormatException nfe) {
				boolValid = false;
			}

			if (n < nLow || n > nHigh) {
				boolValid = false;
			}

			if (boolValid) {
				ov.setValue(n);
				l.setText("");

			} else {
				l.setText("invalid value -- please try again...");
			}

			return true;
		}

		return false;
	}

	@SuppressWarnings("deprecation")
	public boolean handleEvent(Event evt) {

		if (evt.id == Event.WINDOW_DESTROY) {
			ov.deleteObserver(this);
			dispose();
			return true;
		}   

		return super.handleEvent(evt);
	}

	public void update(Observable obs, Object obj) {
		if (obs == ov) {
			tf.setText(String.valueOf(ov.getValue()));
		}
	}
}
