package dat251.observer.solution3;
/*
 * Konto.java
 *
 * Created on 10. september 2002, 12:42
 * Representerer en bankkonto der bel�p kan tas ut og settes inn
 * Dette er modellen i MVC
 */


/**
 *
 * @author  anderl
 * @version 1.1
 */
import java.util.Observable;

public class Konto extends Observable {
    // Konto balansen
    private double saldo;
    
    //kontonavn, ikke til oppdatering
    private String kontoNavn;
    
    /** Creates new Konto */
    public Konto(String n, double bel�p) {
        kontoNavn = n;
        saldo = bel�p;
    }

    private void setSaldo(double sal) {
        saldo = sal;
        
        // kall setChanged f�r notifyObservers
        // indikerer at modellen er endret
        setChanged();  // klassen Observable
        
        // gi melding til Observer-objektene
        notifyObservers();
    }
    
    public double getSaldo() {
        return saldo;
    }
    
    public void taUtBel�p(double bel�p) 
                 throws IllegalArgumentException {
                     
        if (bel�p < 0) throw new IllegalArgumentException(
                       "Kan ikke ta ut et negativt bel�p");
        //oppdater saldo
        setSaldo( getSaldo() - bel�p );
    }   
    
    public void settInnBel�p(double bel�p)
                 throws IllegalArgumentException {
                     
        if (bel�p < 0) throw new IllegalArgumentException(
                       "Kan ikke sette inn et negativt bel�p");
        //oppdater saldo 
        setSaldo( getSaldo() + bel�p );
    }    
    
    public String getKontoNavn() {
        return kontoNavn;
    }
}
