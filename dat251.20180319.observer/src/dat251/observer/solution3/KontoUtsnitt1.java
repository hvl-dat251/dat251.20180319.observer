package dat251.observer.solution3;
/*
 * KontoUtsnitt1.java
 *
 * Created on 10. september 2002, 13:20
 */


/**
 *
 * @author  anderl
 * @version 
 */
import java.util.*;
import javax.swing.*;
import java.text.DecimalFormat;

public class KontoUtsnitt1 extends AbstractKontoUtsnitt {

    private JTextField saldoTekstFelt = new JTextField(10);
    private DecimalFormat toSiffer = new DecimalFormat("0.00");
    
    /** Creates new KontoUtsnitt1 */
    public KontoUtsnitt1(Konto konto) {
        super(konto);
        // ikke oppdatering av tekstfelt
        saldoTekstFelt.setEditable(false);
        
        //legg ut komponentene
        add(new JLabel("View 1 viser saldo: "));
        add(saldoTekstFelt);
        updateDisplay();
    }
    
    public void updateDisplay() {
        // hent saldo med metoden getSaldo som
        //arves fra AbstractKontoUtsnitt
        saldoTekstFelt.setText(
             toSiffer.format(getKonto().getSaldo()));
    }

}
