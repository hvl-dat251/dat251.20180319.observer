package dat251.observer.solution2;

public class Main {

	public static void main(String[] args) {

		ObservableValue ov = new ObservableValue(50, 0, 100);
		TextObserver to = new TextObserver(ov);
		ScrollObserver so = new ScrollObserver(ov);

		ov.addObserver(to);
		ov.addObserver(so);

		for (int i = 0; i < 10; i++) {
			ov.setValue(i * 10);
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
			}
		}
	}
}
