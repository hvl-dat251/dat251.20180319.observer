package dat251.observer.solution3;
/*
 * KontoKontroller.java
 *
 * Created on 10. september 2002, 14:06
 */


/**
 *
 * @author  anderl
 * @version 
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class KontoKontroller extends JPanel {

    private Konto konto;
    private JTextField bel�pFelt;
    
    /** Creates new KontoKontroller */
    public KontoKontroller(Konto kont) {
        super();
        konto = kont;
        bel�pFelt = new JTextField(10);
        
        JButton taUtKnapp = new JButton("Ta ut");
        taUtKnapp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) { 
                try {
                    konto.taUtBel�p(Double.parseDouble(
                        bel�pFelt.getText()));
                }
                catch(NumberFormatException ne) {
                    JOptionPane.showMessageDialog(KontoKontroller.this,
                         "Vennligst oppgi et gyldig bel�p",  
                         "Feil", JOptionPane.ERROR_MESSAGE);
                }
                catch(IllegalArgumentException ine) {
                    JOptionPane.showMessageDialog(KontoKontroller.this,
                         ine.getMessage(),  
                         "Feil", JOptionPane.ERROR_MESSAGE);  
                }
            }
          }
        );
        
        JButton settInnKnapp = new JButton("Sett inn");
        settInnKnapp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) { 
                try {
                    konto.settInnBel�p(Double.parseDouble(
                        bel�pFelt.getText()));
                }
                catch(NumberFormatException ne) {
                    JOptionPane.showMessageDialog(KontoKontroller.this,
                         "Vennligst oppgi et gyldig bel�p",  
                         "Feil", JOptionPane.ERROR_MESSAGE);
                }
                catch(IllegalArgumentException ine) {
                    JOptionPane.showMessageDialog(KontoKontroller.this,
                         ine.getMessage(),  
                         "Feil", JOptionPane.ERROR_MESSAGE);  
                }                
            }
          }
        ); 
        
        setLayout(new FlowLayout());
        add (new JLabel("Bel�p:"));
        add(bel�pFelt);
        add(taUtKnapp);
        add(settInnKnapp);
    }

}
